<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario de registro</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilo.css">
    <script src="js/efecto.js"></script>
</head>

<body>
    <h1>Formulario de registro</h1>
    <form method="POST" action="">
        <div class="row mb-3">
            <div class="col">
                <label for="inputnom">Nombre</label>
                <input type="text" id="inputnom" name="nombre" class="form-control" placeholder="First name">
            </div>
            <div class="col">
                <label for="inputapel">Apellido</label>
                <input type="text" id="inputapel" name="apellido"  class="form-control" placeholder="Last name">
            </div>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email </label>
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="example@gmail.com">

        </div>

        <div class="form-group">
            <label for="inputtel">Telefono</label>
            <input type="text" name="telefono" class="form-control" id="inputtel" placeholder="8888-8888">
        </div>


        <button type="submit" name="register" class="btn btn-primary">Registrar</button>
        <button type="reset" name="reset" class="btn btn-primary" >Limpiar</button>
    </form>
    <?php 
        include("registrar.php");
        ?>
</body>

</html>